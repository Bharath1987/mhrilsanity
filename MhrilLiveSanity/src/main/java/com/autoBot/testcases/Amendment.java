package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.pages.UpcomingHolidays;
import com.autoBot.testng.api.base.Annotations;

public class Amendment extends Annotations {
	@BeforeTest
	public void setData() {
		testcaseName = "Amendment";
		testcaseDec = "MHRIL Sanity testing";
		author = "Bharath";
		category = "sanity";
		excelFileName = "TC001";

	} 
	@Test(dataProvider="fetchData")
	public void SanityTesting(String MemberID, String pwd, String Prsntrvlr, String Flow) throws InterruptedException {
		new LoginPage()
		.MemberID(MemberID)
		.Password(pwd)
		.LoginButton2()
		.ClickViewAllBookings()
		.Modify()
		.fileRead()
		.Modify();
		
		
	}

}
