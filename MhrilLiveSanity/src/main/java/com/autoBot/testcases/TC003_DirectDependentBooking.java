package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC003_DirectDependentBooking {
	public class TC002_CoApplicantbooking extends Annotations {
		@BeforeTest
		public void setData() {
			testcaseName = "TC003_DirectDependentBooking";
			testcaseDec = "MHRIL Sanity testing";
			author = "Bharath";
			category = "sanity";
			excelFileName = "TC003";
		} 

		@Test(dataProvider="fetchData") 
		public void SanityTesting(String MemberID, String pwd, String Prsntrvlr) throws InterruptedException {
			new LoginPage()
			.MemberID(MemberID)
			.Password(pwd)
			.LoginButton()
			.PersonTravelling(Prsntrvlr)
			.ShowResortbutton()
			.ClickSelectRoomsbtn()
			.RoomSelection()

			//.BookingProceedbtn()
			//.minimumnightruleoverride()
			.ClickConfirmBookingbtn()
			.VerifyConfirmedBooking();




			//.clickLogout();

		}
	}
}
