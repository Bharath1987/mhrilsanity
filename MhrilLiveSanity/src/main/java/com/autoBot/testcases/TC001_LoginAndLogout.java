package com.autoBot.testcases;

import java.io.FileNotFoundException;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC001_LoginAndLogout extends Annotations{

	@BeforeTest
	public void setData() {
		testcaseName = "TC001_LoginAndLogout";
		testcaseDec = "MHRIL Sanity testing";
		author = "Bharath";
		category = "sanity";
		excelFileName = "TC001";

	} 

	@Test(dataProvider="fetchData") 
	public void SanityTesting(String MemberID, String pwd, String Prsntrvlr, String Flow) throws InterruptedException, FileNotFoundException {
		new LoginPage()
		.MemberID(MemberID)
		.Password(pwd)
		.GetString(Flow)
		.PersonTravelling(Prsntrvlr)
		.ShowResortbutton()
		.ClickSelectRoomsbtn()
		.RoomSelection()

		//.BookingProceedbtn()
		//.minimumnightruleoverride()
		.ClickConfirmBookingbtn()
		.VerifyConfirmedBooking()
		.usingBufferWriter()
		
		.ClickViewAllBookings()
		.fileRead()
		//.ClickViewAllBookings()
		.Modify();
		




		//.Logout();

	}

}






