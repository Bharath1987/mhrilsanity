package com.autoBot.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.autoBot.testng.api.base.Annotations;

public class MyDashboard extends Annotations{


	
	public MyDashboard PersonTravelling(String prsntrvlr) throws InterruptedException {
		//		Alert alert = driver.switchTo().alert();
		//		alert.dismiss();
		//		int y = driver.findElementById("personTravling")
		//				.getLocation().getY();

		WebElement enterDestination = driver.findElement(By.id("personTravling"));

		// Way 1										   //x,y
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", enterDestination);
		Thread.sleep(5000);
		WebElement PersonTravel = locateElement("Id", "personTravling");
		PersonTravel.getText();
		System.out.println(PersonTravel);
		selectDropDownUsingText(PersonTravel, prsntrvlr);

		//			 Select persontravelling =new Select(driver.findElement(By.id("personTravling")));
		//			      persontravelling.selectByVisibleText("Member"); 
		//			 

		return this;

	}
	public ResortList ShowResortbutton() {
		WebElement ShowResortbutton = locateElement("Id", "show_resorts");
		click(ShowResortbutton);
		return new ResortList();

	}
	public Cancellation ViewAllBooking() throws InterruptedException {
		WebElement ClickViewAllBookingsbtn = locateElement("class", "view-all-bookings");
		click(ClickViewAllBookingsbtn);
		return new Cancellation();
	}
	
}
