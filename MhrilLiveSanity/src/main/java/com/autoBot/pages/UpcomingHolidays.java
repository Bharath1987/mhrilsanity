package com.autoBot.pages;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.autoBot.testng.api.base.Annotations;
import com.autoBot.pages.*;

public class UpcomingHolidays extends Annotations {

	String fileName="MyFile.txt";
	public String line;
	WebDriverWait wb=new WebDriverWait(driver, 30);
	public UpcomingHolidays ClickViewAllBookings() throws InterruptedException {
		//driver.navigate().refresh();
		WebElement ClickViewAllBookingsbtn = locateElement("class", "view-all-bookings");
		click(ClickViewAllBookingsbtn);
		
		return this;
		
	}
	
	public UpcomingHolidays fileRead() throws InterruptedException {
		
		try {
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			try {
				while((line = br.readLine())!= null) {
					System.out.println("Reading CV from text file:" +line);
					Modify();
				}
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}} catch (FileNotFoundException e) {
				e.printStackTrace();
				
}
		return this;
}
	
	public UpcomingHolidays Modify() throws InterruptedException {
		
		//ConfirmedBookingScreen ss=new ConfirmedBookingScreen();
	
//		ResortList obj = new ResortList();
//		BookingSummary Date = obj.RoomSelection();
		
		//String attribute1 = ResortList.attribute;
		
		
		
//		Outer:for(int i=0; i<3;) {
//			try {
//			WebElement NoHolidays = driver.findElement(By.xpath("//*[@class='no-holi']")); 
//			//WebElement Modify = driver.findElement(By.xpath("/a[text()='Modify']")); 
//			//Thread.sleep(5000);
//			if(NoHolidays.isDisplayed()){
//				
//				driver.findElement(By.xpath("//li[@type='upcoming']//a[1]")).click();
//				
//				i++;
//		}
//			else {
//				break Outer;
//			}
//			
//		
//			}
//		
//		catch(Exception e) {
//			//e.printStackTrace();
//		}
//		}
		
		
		
		
		//wb.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/a[text()='Modify']")));
		List < WebElement > findElements = driver.findElements(By.xpath("//a[text()='Modify']"));
		int size = findElements.size();
		System.out.println(size);
		for(int i=1; i<=size;) {
			System.out.println("Inside for loop");
			WebElement findElements2 = driver.findElement(By.xpath("//a[text()='Modify'][" + i + "]"));
			findElements2.click();
		
			//int i1=Integer.parseInt(Vouchernumber1); 
			
			
			
			WebElement findElement = driver.findElement(By.className("resort-cv-number"));
			
			String Vouchernumber2 = findElement.getText().toString();
			//int i2=Integer.parseInt(Vouchernumber2); 
			System.out.println(Vouchernumber2);
			//Vouchernumber1.equals(Vouchernumber2); 
			System.out.println(line);

			if(line.equals(Vouchernumber2)) {
				System.out.println("Inside for If loop");
				driver.findElementById("booking_proceed").click();
				driver.findElementById("modify-proceed").click();
				driver.findElementById("modify-booking-button").click();
				break;
				
			}
			else
			{
				System.out.println("Inside for else loop");
				driver.navigate().back();
				i++;
			}
		}
			
		
		
		return this;
		
	}
	
		
//	public void CalculateInventory() {
//		WebElement CalculateInventoryBtn = locateElement("xpath", "//a[text()='Calculate Inventory']");
//		click(CalculateInventoryBtn);
//		if(driver.findElementByClassName("can-purcv inner").click()) {
//			
//		}
//	}
	
	}


