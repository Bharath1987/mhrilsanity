package com.autoBot.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.autoBot.testng.api.base.Annotations;

public class LoginPage extends Annotations {
	
	public LoginPage MemberID(String memberID) {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		try {
			driver.findElementById("acceptCookie").click();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		WebElement EnterMemberID = locateElement("Id", "username");
		clearAndType(EnterMemberID, memberID);
		return this;

	}

	public LoginPage Password(String Pwd) {
		WebElement Password = locateElement("Id", "password");
		clearAndType(Password, Pwd);
		return this;

	}
	
	public MyDashboard GetString(String Flow) throws InterruptedException {
		String S = Flow;
		if(S.equalsIgnoreCase("Booking")) {
			System.out.println(S);
			LoginButton1();
		}
		else {
			LoginButton2();
			
		}
		return new MyDashboard();
	}

	
	public MyDashboard LoginButton1() throws InterruptedException {
		WebElement LoginButton = locateElement("Id", "login");
		click(LoginButton);
		Thread.sleep(5000);
		
		return new MyDashboard();

	}
	public UpcomingHolidaysForCancel LoginButton2() {
		WebElement LoginButton = locateElement("Id", "login");
		click(LoginButton);
		WebDriverWait wb=new WebDriverWait(driver, 30);
		wb.until(ExpectedConditions.visibilityOfElementLocated(By.className("view-all-bookings")));
		
		return new UpcomingHolidaysForCancel();

	}
}
