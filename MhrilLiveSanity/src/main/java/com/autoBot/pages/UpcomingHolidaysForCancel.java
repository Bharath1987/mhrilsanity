package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class UpcomingHolidaysForCancel extends Annotations {
	public Cancellation ClickViewAllBookings1() throws InterruptedException {
		//driver.navigate().refresh();
		WebElement ClickViewAllBookingsbtn = locateElement("class", "view-all-bookings");
		click(ClickViewAllBookingsbtn);
		
		return new Cancellation();
		
	}
	
}
