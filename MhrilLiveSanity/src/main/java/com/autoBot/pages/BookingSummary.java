package com.autoBot.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class BookingSummary extends Annotations {
	
	public ConfirmedBookingScreen ClickConfirmBookingbtn() throws InterruptedException {
		Thread.sleep(5000);
		//((JavascriptExecutor) driver).executeScript("scroll(500);");
		((JavascriptExecutor) driver).executeScript("window.scrollTo(0, document.body.scrollHeight)"); 
			WebElement ClickConfirmBookingbtn = locateElement("xpath", "//div[@class='right_sec']//a[1]");
			click(ClickConfirmBookingbtn);
		
		return new ConfirmedBookingScreen();
		
		
	}

}
