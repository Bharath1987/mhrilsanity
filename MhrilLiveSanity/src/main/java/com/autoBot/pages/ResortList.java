package com.autoBot.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.autoBot.testng.api.base.Annotations;

public class ResortList extends Annotations {
	static String attribute;
	public ResortList ClickSelectRoomsbtn() throws InterruptedException {
		//		Alert alert = driver.switchTo().alert();
		//		alert.accept();
		WebElement SelectRooms = driver.findElement(By.className("result"));
		// Way 1 //x,y
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", SelectRooms);
		Thread.sleep(5000);
		WebElement ClickSelectRoomsbutton = locateElement("Xpath", "(//div[@class='right_sec']//a)[3]");
		click(ClickSelectRoomsbutton);
		return this;
	}

	//@SuppressWarnings("unlikely-arg-type")
	public BookingSummary RoomSelection() throws InterruptedException {
		//((JavascriptExecutor)driver).executeScript("scroll(0,400)"); 
		//String S ="Please Enter Co Applicant Details ";
		Outer: for (int i = 1; i <= 4;) {
			List < WebElement > findElement = driver.findElements(By.xpath("//div[@class='available'][" + i + "]/div"));
			//  System.out.println("find element size" + findElement.size());
			Inner: for (int j = 1; j <= findElement.size();) {
				System.out.println("Inside Second Loop: " + j);
				System.out.println(findElement.size());
				WebElement text = driver.findElement(By.xpath("//div[@class='available'][" + i + "]/div[" + j + "]"));
				String text2 = text.getText();
				//System.out.println(text2);
				Thread.sleep(2000);
				Parent:if (text2.contains("AVL")) {
					System.out.println("Inside text2 if");
					text.click();
					
					try {
					WebElement displayed = driver.findElementByXPath("//p[text()='Check-in in 1Up higher season is not allowed. Please select alternate dates.']");
					if(displayed.isDisplayed()) {
						driver.findElementByXPath("//a[@class='btn btn_ok']").click();
						j++;
						System.out.println(j);
						continue;
						
					}
					}
					catch (Exception e) {
						// e.printStackTrace();
					}
							
				
					
					attribute = text.getAttribute("grid_date");
					System.out.println(attribute);
					WebElement ContinueBookingBtn = driver.findElement(By.id("booking_proceed"));
					Thread.sleep(2000);
					click(ContinueBookingBtn);
					
					Thread.sleep(2000);
					WebElement Aptrule = driver.findElement(By.xpath("//div[@id='error_box_popup']//li[1]"));
					String Ruletxt = Aptrule.getText();
					
					if (Ruletxt.contains("exceeds the maximum apartment")||Ruletxt.contains("holiday reservation overlapping")) {
						driver.findElementByXPath("(//a[text()='OK'])[1]").click();
						driver.findElementByXPath("//span[text()='clear selection ']").click();
						Thread.sleep(3000);
						System.out.println("Moving to j++");
						j++;
					} 
					
					else {
						System.out.println("inside else block");
						WebElement ClickOK = locateElement("Xpath", "(//a[text()='OK'])[1]");
						click(ClickOK);
						
					}
						driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
						try {
							WebElement Coapp = driver.findElementByXPath("//h6[text()='Please Enter Co Applicant Details ']");
							System.out.println(Coapp);
							if(Coapp.isDisplayed()) {

								driver.findElementByXPath("//input[@value='submit']").click();
							}
						}
						catch (Exception e) {
							// e.printStackTrace();
						}
						try {
							WebElement DirectDependent = driver.findElementByXPath("//h6[text()='Please enter direct dependent details ']");
							if(DirectDependent.isDisplayed()) {
								WebElement SelectDirectDependent = locateElement("Id", "relation");
								SelectDirectDependent.getText();
								System.out.println(SelectDirectDependent);
								selectDropDownUsingText(SelectDirectDependent, "Father");
								WebElement DDSubmitclick = locateElement("Xpath", "//input[@value='submit']");
								click(DDSubmitclick);
							}
						}
						catch (Exception e) {
							// e.printStackTrace();
						}        	
					
					
				} else if (text2.contains("WL") || text2.contains("NA")) {
					System.out.println("text2 else if");
					i++;
					break Inner;
				}
				break Outer;
			}
			
		}
	return new BookingSummary();
	}
	//
	//	public BookingSummary minimumnightruleoverride() {
	//		WebElement ClickOK = locateElement("Id", "continue_to_next_step");
	//		click(ClickOK);
	//		return new BookingSummary();
	//
	//	}
	//

	// public BookingSummary DDSubmitclick() {
	//	 
	//	 WebElement DDSubmitclick = locateElement("Xpath", "//input[@value='submit']");
	//	  click(DDSubmitclick);
	//	
	//	 
	//	return new BookingSummary();
	//		WebElement ClickOK = locateElement("Id", "continue_to_next_step");
	//		click(ClickOK);
	//		return new BookingSummary();
}
