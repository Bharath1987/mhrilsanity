package com.autoBot.pages;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.autoBot.testng.api.base.Annotations;
import com.autoBot.pages.*;


public class ConfirmedBookingScreen extends Annotations{
	public static WebElement ClickConfirmBookingbtn; 
	public static String vouchernumber="";
	//Common cs = new Common();
	public ConfirmedBookingScreen VerifyConfirmedBooking() {
		 ClickConfirmBookingbtn = locateElement("Class", "voucher-code");
		 vouchernumber = ClickConfirmBookingbtn.getText();
		
		System.out.println("Voucher Number after booking is "+ vouchernumber);
		
		return this;

	}	
	
	String fileName ="MyFile.txt";
	@Test(priority=1)
	public UpcomingHolidays usingBufferWriter() {
		FileWriter fw;
		try {
			fw = new FileWriter(fileName);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.flush();
			bw.write(vouchernumber);
			//bw.newLine();
			
			//bw.write("TestLeaf Solutions Private Limited");
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		 return new UpcomingHolidays();
	}
	
	public LoginPage Logout() {
		
		WebElement ClickLogout = locateElement("Class", "member_name");
		click(ClickLogout);
		
		return new LoginPage();

	}

}
